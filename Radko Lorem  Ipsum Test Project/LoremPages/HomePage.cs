﻿using OpenQA.Selenium;
using Radko_Lorem__Ipsum_Test_Project.BLL;
using Radko_Lorem__Ipsum_Test_Project.LoremPages;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Radko_Lorem__Ipsum_Test_Project.PageObjects
{
    public class HomePage
    {
        [FindsBy(How = How.XPath, Using = "//a[@class='ru']")]
        private IWebElement russianLanguageItem;

        [FindsBy(How = How.XPath, Using = "//div[@id='Panes']/div[1]")]
        private IWebElement firstParagraph;

        [FindsBy(How = How.XPath, Using = "//input[@id='generate']")]
        private IWebElement generateButton;

        [FindsBy(How = How.XPath, Using = "//input[@id='amount']")]
        private IWebElement numberFieldTextbox;

        [FindsBy(How = How.XPath, Using = "//input[@id='start']")]
        private IWebElement startCheckbox;

        private RadioButton radioButton;

        public HomePage()
        {
            IWebDriver driver = TechLayer.GetDriver();
            radioButton = new RadioButton(driver);
            PageFactory.InitElements(driver, this);
        }

        public void ChangeLanguageIntoRussian()
        { russianLanguageItem.Click(); }

        public string GetFirstParagraphText()
        {  return firstParagraph.Text; }

        public void ClickGenerateButton()
        {  generateButton.Click(); }

        public IWebElement GetNumberFieldTextbox()
        { return numberFieldTextbox; }

        public void ClickRadioButton(string value)
        { radioButton.SetValue(value); }

        public void ClickWordsRadioButton()
        { radioButton.SetValue("words"); }

        public void ClickBytesRadioButton()
        { radioButton.SetValue("bytes");  }

        public void UncheckStartCheckbox()
        { startCheckbox.Click(); }

    }
}

