﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Radko_Lorem__Ipsum_Test_Project.LoremPages
{
    class RadioButton
    {
        [FindsBy(How = How.XPath, Using = "//input[@type='radio']")]
        private IList <IWebElement> radioButtons;

        public RadioButton (IWebDriver driver)
        {
            PageFactory.InitElements(driver, this);
        }

        public void SetValue(string value)
        {
            foreach (IWebElement radioButton in radioButtons)
            {
                if (radioButton.GetAttribute("value").Equals(value))
                {
                    radioButton.Click();
                }
            }
           
	    }

    }
}
