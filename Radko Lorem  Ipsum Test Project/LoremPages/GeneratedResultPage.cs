﻿using OpenQA.Selenium;
using Radko_Lorem__Ipsum_Test_Project.BLL;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Radko_Lorem__Ipsum_Test_Project.PageObjects
{
    public class GeneratedResultPage
    {
        private readonly IWebDriver driver;

        [FindsBy(How = How.XPath, Using = "//div[@id='lipsum']/p[1]")]
        private IWebElement firstGeneratedParagraph;

        [FindsBy(How = How.XPath, Using = "//div[@id='lipsum']/p")]
        private IWebElement allGeneratedText;

        [FindsBy(How = How.XPath, Using = "//div[@id='lipsum']/p")]
        private IList<IWebElement> allGeneratedParagraphs;

        public GeneratedResultPage()
        {
           this.driver = TechLayer.GetDriver();
           PageFactory.InitElements(driver, this);
        }

        public string GetFirstGeneratedParagraphText()
        { return firstGeneratedParagraph.Text; }

        public string GetAllGeneratedText()
        { return allGeneratedText.Text; }

        public IEnumerable <string> AllGeneratedParagraph()
        { return allGeneratedParagraphs.Select(element => element.Text) ; }

        public void NavigateToHomePage()
        { driver.Navigate().GoToUrl(TechLayer.LIPSUM_URL); }
    }
}

