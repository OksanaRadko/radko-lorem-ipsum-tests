﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace Radko_Lorem__Ipsum_Test_Project.PageObjects
{
    [TestClass]
    public class CheckGenerationTests : BaseTest
    {
        private int ENTERED_QUERY = 10;
        private int NEGATIVE_ENTERED_QUERY = -1;
        private int DEFAULT_COUNT_OF_ELEMENTS = 5;

        [TestMethod]
        public void CheckThatGenerateButtonWorks()
        {
            string firstParagraph = GetBLL().ClickGenerateButtonAndReturnFirstParagraphText();
            Assert.IsTrue(firstParagraph.StartsWith("Lorem ipsum dolor sit amet, consectetur adipiscing elit"), "The first paragraph doesn`t start with 'Lorem ipsum dolor sit amet, consectetur adipiscing elit'");
        }

        [TestMethod]
        public void CheckThatGenerationByWordsWorks()
        {
            string result = GetBLL().GenerationByWords(ENTERED_QUERY.ToString());
            string[] wordsCount = result.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            int countOfWords = wordsCount.Length;
            Assert.AreEqual(countOfWords, ENTERED_QUERY, "Actual count of words is not equal to entered count of words");
        }

        [TestMethod]
        public void NegativeTestForGenerationByWords()
        {
            string result = GetBLL().GenerationByWords(NEGATIVE_ENTERED_QUERY.ToString());
            string[] wordsCount = result.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            int countOfWords = wordsCount.Length;
            Assert.AreEqual(countOfWords, DEFAULT_COUNT_OF_ELEMENTS, "Generated number of words cannot be negative");
        }

        [TestMethod]
        public void CheckThatGenerationByBytesWorks()
        {
            string result = GetBLL().GenerationByBytes(ENTERED_QUERY.ToString());
            int countOfBytes = result.Length;
            Assert.AreEqual(ENTERED_QUERY, countOfBytes, "Actual count of bytes is not equal to entered count of bytes");
        }

        [TestMethod]
        public void NegativeTestForGenerationByBytes()
        {
            string result = GetBLL().GenerationByBytes(NEGATIVE_ENTERED_QUERY.ToString());
            int countOfBytes = result.Length;
            Assert.AreEqual(countOfBytes, DEFAULT_COUNT_OF_ELEMENTS, "Generated number of bytes cannot be negative");
        }


        [TestMethod]
        public void CheckGenerationByStartCheckboxWorks()
        {
            string generatedByCheckboxText = GetBLL().GenerationByUncheckStartCheckbox();
            Assert.IsFalse(generatedByCheckboxText.StartsWith("Lorem ipsum"), "Generated result shouldn`t start with 'Lorem ipsum'");
        }

        [TestMethod]
        public void CheckRandomlyGeneratedParagraphs()
        { 
            int totalCount = 0;
            for (int i = 0; i < ENTERED_QUERY; i++)
            {
                IEnumerable<string> paragraphs = GetBLL().GenerateParagraphsListRandomlyAndReturnToHomePage();            
                int count = 0;
                foreach (string paragraph in paragraphs)
                {
                    if (paragraph.Contains("lorem"))
                        count++;
                }
                totalCount += count;
            }

            decimal averageCount = totalCount / ENTERED_QUERY;

            Assert.IsTrue(averageCount >= 2 || averageCount < 3, "Average count result should be between 2 and 3");
        }
    }
}