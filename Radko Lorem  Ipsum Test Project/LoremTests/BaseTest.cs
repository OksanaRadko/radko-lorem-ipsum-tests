﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using Radko_Lorem__Ipsum_Test_Project.BLL;

namespace Radko_Lorem__Ipsum_Test_Project.PageObjects
{
    [TestClass]
    public class BaseTest
    {
        protected readonly LoremIpsumBLL bll = new LoremIpsumBLL();

        [TestInitialize]
        public void TestsSetUp()
        { TechLayer.Init(); }

        [TestCleanup]
        public void TearDown()
        { TechLayer.Clear(); }

        protected LoremIpsumBLL GetBLL() 
        { return bll; }
    }
}
