﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace Radko_Lorem__Ipsum_Test_Project.PageObjects
{
    [TestClass] 
    public class OtherLoremIpsumTests : BaseTest
    {
        [TestMethod]
        public void CheckThatLanguageIsChangedIntoRussian()
        {
            string firstParagraph = GetBLL().ChangeLanguageIntoRussianAndReturnFirstParagraphText();
            Assert.IsTrue(firstParagraph.Contains("рыба"), "The first paragraph doesn`t contain the word 'рыба'");
        }
    }
}
