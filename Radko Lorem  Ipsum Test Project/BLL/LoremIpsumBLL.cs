﻿using Radko_Lorem__Ipsum_Test_Project.PageObjects;
using System.Collections;
using System.Collections.Generic;

namespace Radko_Lorem__Ipsum_Test_Project.BLL
{
    public class LoremIpsumBLL
    {
        private HomePage GetHomePage()
        { return new HomePage(); }

        private GeneratedResultPage GetGeneratedResultPage()
        { return new GeneratedResultPage(); }

        public string ChangeLanguageIntoRussianAndReturnFirstParagraphText()
        {
            GetHomePage().ChangeLanguageIntoRussian();
            return GetHomePage().GetFirstParagraphText();
        }
        public string ClickGenerateButtonAndReturnFirstParagraphText() 
        {
            GetHomePage().ClickGenerateButton();
            return GetGeneratedResultPage().GetFirstGeneratedParagraphText();
        }

        public string GenerationByWords(string query) 
        {
            GetHomePage().GetNumberFieldTextbox().Clear();
            GetHomePage().GetNumberFieldTextbox().SendKeys(query);
            GetHomePage().ClickWordsRadioButton();
            GetHomePage().ClickGenerateButton();
            return GetGeneratedResultPage().GetAllGeneratedText();
        }

        public string GenerationByBytes(string query)
        {
            GetHomePage().GetNumberFieldTextbox().Clear();
            GetHomePage().GetNumberFieldTextbox().SendKeys(query);
            GetHomePage().ClickBytesRadioButton();
            GetHomePage().ClickGenerateButton();
            return GetGeneratedResultPage().GetAllGeneratedText();
        }

        public string GenerationByUncheckStartCheckbox()
        {
            GetHomePage().UncheckStartCheckbox();
            GetHomePage().ClickGenerateButton();
            return GetGeneratedResultPage().GetAllGeneratedText(); 
        }

        public IEnumerable<string> GenerateParagraphsListRandomlyAndReturnToHomePage()
        {
            GetHomePage().ClickGenerateButton();
            IEnumerable<string> result = GetGeneratedResultPage().AllGeneratedParagraph();
            GetGeneratedResultPage().NavigateToHomePage();
            return result;
        }
}
}
