﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Radko_Lorem__Ipsum_Test_Project.BLL
{
    class TechLayer
    {
        private static IWebDriver driver;
        public static readonly string LIPSUM_URL = "https://lipsum.com/";

        public static IWebDriver GetDriver() 
        { return driver; }

        public static void Init() 
        {
            driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl(LIPSUM_URL);
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
        }

        public static void Clear() 
        {driver.Close(); }
    }
}
