﻿Feature: 

@mytag
Scenario: Check that language is changed into russian
	Given LoremIpsum home page is open
	When I click russian language item
	Then the result should contain the word "рыба"
	
Scenario: Check that generate button works
	Given LoremIpsum home page is open
	When I click generate button
	Then the result should starts with "Lorem ipsum dolor sit amet, consectetur adipiscing elit"

Scenario: Check that generation by words works
	Given LoremIpsum home page is open
	When I set text in text field "10"
	And I select radio button "words"
	And I click generate button
	Then the result should contain 10 words

Scenario: Negative test for generation by words
	Given LoremIpsum home page is open
	When I set text in text field "-1"
	And I select radio button "words"
	And I click generate button
	Then the result should contain 5 words

Scenario: Check that generation by bytes works
	Given LoremIpsum home page is open
	When I set text in text field "10"
	And I select radio button "bytes"
	And I click generate button
	Then the result should contain 10 bytes

Scenario: Negative test for generation by bytes
	Given LoremIpsum home page is open
	When I set text in text field "-1"
	And I select radio button "bytes"
	And I click generate button
	Then the result should contain 5 bytes

Scenario: Check generation by start checkbox works
	Given LoremIpsum home page is open
	When I click uncheck start checkbox
	And I click generate button
	Then the result shouldn`t start with "Lorem ipsum"	 
	 
Scenario: Check randomly generated paragraphs
	Given LoremIpsum home page is open
	When I run generate paragraphs 10 times
	And count number of paragraphs that contain "lorem"
	Then average number should be between 2 and 3

