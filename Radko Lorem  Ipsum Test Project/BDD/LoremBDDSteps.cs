﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Radko_Lorem__Ipsum_Test_Project.BLL;
using Radko_Lorem__Ipsum_Test_Project.PageObjects;
using System;
using System.Collections.Generic;
using TechTalk.SpecFlow;

namespace Radko_Lorem__Ipsum_Test_Project.LoremTests
{
    [Binding]
    public class CheckGenerationFeatureSteps
    {
        private HomePage GetHomePage()
        { return new HomePage(); }

        private GeneratedResultPage GetGeneratedResultPage()
        { return new GeneratedResultPage(); }

        [AfterScenario]
        public void Clear() {
            TechLayer.Clear();
        }

        [Given(@"LoremIpsum home page is open")]
        public void GivenLoremIpsumHomePageIsOpen()
        {           
                TechLayer.Init();
        }

        [When(@"I click russian language item")]
        public void WhenIClickRussianLanguageItem()
        {
            GetHomePage().ChangeLanguageIntoRussian();
        }

        [Then(@"the result should contain the word ""(.*)""")]
        public void ThenTheResultShouldContainTheWord(string expectedResult)
        {
            string firstParagraph = GetHomePage().GetFirstParagraphText();
            Assert.IsTrue(firstParagraph.Contains(expectedResult), "The first paragraph doesn`t contain the word 'рыба'");
        }
        
        [When(@"I click generate button")]
        public void WhenIClickGenerateButton()
        {
            GetHomePage().ClickGenerateButton();
        }
        
        [Then(@"the result should starts with ""(.*)""")]
        public void ThenTheResultShouldStartsWith(string expectedResult)
        {
            string firstParagraphText = GetGeneratedResultPage().GetFirstGeneratedParagraphText();
            Assert.IsTrue(firstParagraphText.StartsWith(expectedResult), "The first paragraph doesn`t start with 'Lorem ipsum dolor sit amet, consectetur adipiscing elit'");
        }

        [When(@"I set text in text field ""(.*)""")]
        public void WhenISetTextInTextField(int query)
        {
            GetHomePage().GetNumberFieldTextbox().Clear();
            GetHomePage().GetNumberFieldTextbox().SendKeys(query.ToString());
        }

        [When(@"I select radio button ""(.*)""")]
        public void WhenISelectRadioButton(string value)
        {
            GetHomePage().ClickRadioButton(value);
        }

        [Then(@"the result should contain (.*) words")]
        public void ThenTheResultShouldContainWords(int expectedWordsCount)
        {
            string text = GetGeneratedResultPage().GetAllGeneratedText();
            string[] wordsCount = text.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            int countOfWords = wordsCount.Length;
            Assert.AreEqual(countOfWords, expectedWordsCount, "Actual count of words is not equal to entered count of words");
        }

        [Then(@"the result should contain (.*) bytes")]
        public void ThenTheResultShouldContainBytes(int expectedBytesCount)
        {
            string text = GetGeneratedResultPage().GetAllGeneratedText();
            int countOfBytes = text.Length;
            Assert.AreEqual(countOfBytes, expectedBytesCount, "Actual count of bytes is not equal to entered count of bytes");
        }

        [When(@"I click uncheck start checkbox")]
        public void WhenIClickUncheckStartCheckbox()
        {
            GetHomePage().UncheckStartCheckbox();
        }

        [Then(@"the result shouldn`t start with ""(.*)""")]
        public void ThenTheResultShouldnTStartWith(string expectedResult)
        {
            string generatedByCheckboxText = GetGeneratedResultPage().GetAllGeneratedText();
            Assert.IsFalse(generatedByCheckboxText.StartsWith(expectedResult), "Generated result shouldn`t start with 'Lorem ipsum'");
        }

        [When(@"I run generate paragraphs (.*) times")]
        public void WhenIRunGenerateParagraphsTimes(int numberOfTimes)
        {
            List<string> result = new List<string>();
            for (int i = 0; i < numberOfTimes; i++) {
                GetHomePage().ClickGenerateButton();
                result.AddRange(GetGeneratedResultPage().AllGeneratedParagraph());
                GetGeneratedResultPage().NavigateToHomePage();
            }
            ScenarioContext.Current.Add("paragraphs", result);
            ScenarioContext.Current.Add("runCount", numberOfTimes);
        }

        [When(@"count number of paragraphs that contain ""(.*)""")]
        public void WhenCountNumberOfParagraphsThatContain(string word)
        {
            List<string> paragraphs = ScenarioContext.Current.Get<List<string>>("paragraphs");
            int count = 0;
            foreach (string paragraph in paragraphs)
            {
                if (paragraph.Contains(word))
                    count++;
            }
            ScenarioContext.Current.Add("wordCount", count);
        }

        [Then(@"average number should be between (.*) and (.*)")]
        public void ThenAverageNumberShouldBeBetweenAnd(int min, int max)
        {
            int totalCount = ScenarioContext.Current.Get<int>("wordCount");
            int runCount = ScenarioContext.Current.Get<int>("runCount");
            decimal averageCount = totalCount / runCount;

            Assert.IsTrue(averageCount >= min || averageCount <= max, "Average count result should be between min and max");
        }
    }
}
